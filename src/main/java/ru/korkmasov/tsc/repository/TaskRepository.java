package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.ITaskRepository;
import ru.korkmasov.tsc.model.Task;
import java.util.ArrayList;
import java.util.List;


public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    public Task findById(final String id){
        for (Task task:list){
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    public Task findByName(final String Name){
        for (Task task:list){
            if (Name.equals(task.getName())) return task;
        }
        return null;
    }

    public Task findByIndex(final int index){
        return list.get(index);
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    public Task removeById(final String id){
        final Task task = findById(id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    public Task removeByName(final String Name){
        final Task task = findByName(Name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    public Task removeByIndex (final int index){
        final Task task = findByIndex(index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

}
