package ru.korkmasov.tsc.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_INFO = "info";

    public static final String CMD_COMMANDS = "commands";

    public static final String CMD_ARGUMENTS = "arguments";

    public static final String CMD_TASK_LIST = "task-list";

    public static final String CMD_TASK_CREATE = "task-create";

    public static final String CMD_TASK_CLEAR = "task-clear";

    public static final String CMD_TASK_SHOW_BY_ID = "task-show-by-id";

    public static final String CMD_TASK_SHOW_BY_INDEX = "task-show-by-index";

    public static final String CMD_TASK_SHOW_BY_NAME = "task-show-by-name";

    public static final String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";

    public static final String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public static final String CMD_TASK_REMOVE_BY_NAME = "task-remove-by-name";

    public static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    public static final String CMD_TASK_START_BY_ID = "task-start-by-id";

    public static final String CMD_TASK_START_BY_INDEX = "task-start-by-index";

    public static final String CMD_TASK_START_BY_NAME = "task-start-by-name";

    public static final String CMD_TASK_FINISH_BY_ID = "task-finish-by-id";

    public static final String CMD_TASK_FINISH_BY_INDEX = "task-finish-by-index";

    public static final String CMD_TASK_FINISH_BY_NAME = "task-finish-by-name";

    public static final String CMD_PROJECT_LIST = "project-list";

    public static final String CMD_PROJECT_CREATE = "project-create";

    public static final String CMD_PROJECT_CLEAR = "project-clear";

    public static final String CMD_PROJECT_SHOW_BY_ID = "project-show-by-id";

    public static final String CMD_PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public static final String CMD_PROJECT_SHOW_BY_NAME = "project-show-by-name";

    public static final String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public static final String CMD_PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    public static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String CMD_PROJECT_START_BY_ID = "project-start-by-id";

    public static final String CMD_PROJECT_START_BY_INDEX = "project-start-by-index";

    public static final String CMD_PROJECT_START_BY_NAME = "project-start-by-name";

    public static final String CMD_PROJECT_FINISH_BY_ID = "project-finish-by-id";

    public static final String CMD_PROJECT_FINISH_BY_INDEX = "project-finish-by-index";

    public static final String CMD_PROJECT_FINISH_BY_NAME = "project-finish-by-name";

}
