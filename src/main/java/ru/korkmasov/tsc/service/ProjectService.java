package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.IProjectRepository;
import ru.korkmasov.tsc.api.IProjectService;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.enumerated.Status;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project updateById(final String id, final String name, final String description){
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project=projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description){
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project=projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Project project = projectRepository.findById(id);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Project project = projectRepository.findByIndex(index);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findByName(name);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Project project = projectRepository.findById(id);
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Project project = projectRepository.findByIndex(index);
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findByName(name);
        project.setStatus(Status.COMPLETED);
        return project;
    }

}
