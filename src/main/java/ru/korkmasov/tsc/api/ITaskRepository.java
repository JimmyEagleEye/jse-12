package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    void add(Task task);

    void remove(Task task);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex (int index);

    void clear();

}
