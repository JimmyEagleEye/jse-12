package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    void add(Project project);

    void remove(Project project);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex (int index);

    void clear();

}
