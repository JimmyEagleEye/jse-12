package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(String name, String description);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    void add(Project project);

    void remove(Project project);

    Project removeById(String id);

    Project removeByName(String Name);

    Project removeByIndex (Integer index);

    void clear();

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

}
